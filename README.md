To use clone the repo using `git clone https://codeberg.org/RustedTerrier/xedoc.git` and then run `cargo build --release && ./target/release/xedoc_render README.xedoc`.

Dependencies:
libc (for finding the terminal size in xedoc_render)
rust nightly

Tested platforms:
Linux

Most UNIX systems should be supported.
