// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
use std::{env, fs, path::Path, process::exit};

use libc::*;

fn main() {
    unsafe {
        // Ensure this program respects SIGPIPE to prevent panicking
        libc::signal(libc::SIGPIPE, libc::SIG_DFL);
    }
    let mut args: Vec<String> = env::args().collect();
    let mut ascii = false;
    let mut size: Option<usize> = None;
    args.remove(0);

    for i in args.iter() {
        if i == "-a" || i == "--ascii" {
            ascii = true;
        } else if i.starts_with("-w") {
            let num = i.trim_start_matches("-w");
            match num.parse::<usize>() {
                | Ok(a) => size = Some(a),
                | Err(_) => eprintln!("Unrecognized flag {}.", i)
            }
        }
    }

    match render(fs::read_to_string(&args[args.len() - 1]).unwrap(), ascii, size) {
        | Err(e) => {
            eprintln!("{}", e);
            exit(1);
        },
        | Ok(a) => println!("{}", a)
    };
}

pub fn render(contents: String, ascii_only: bool, size: Option<usize>) -> Result<String, String> {
    let size = match size {
        | Some(a) => a,
        | None => get_size()
    };

    let mut output = String::new();
    let mut err_output = None;

    let lines: Vec<&str> = contents.split('\n').collect();

    let mut line_syn = Vec::new();

    for i in lines {
        let mut line: Vec<&str> = i.split(' ').collect();
        // l_type is the type of line, ex: `p some text` would make l_type `p` and thus a
        // paragraph-line
        let l_type = line[0];
        line.remove(0);

        let l_content = match line.is_empty() {
            | false => line.join(" "),
            | true => String::new()
        };

        line_syn.push(Line { l_type, l_content });
    }

    // The indentation level is tracked with this variable
    let mut indent = 0;
    let mut j = 0;
    while j < line_syn.len() {
        let mut current_line = line_syn[j].clone();

        // If a table is started, step through until the table is ended and then render all of it
        if current_line.l_type == "s" {
            let mut table_syn = Vec::new();
            while current_line.l_type != "q" {
                j += 1;
                current_line = line_syn[j].clone();
                table_syn.push(current_line.clone());
            }
            table_syn.pop();
            match render_table(table_syn, indent, ascii_only) {
                | Ok(a) => output.push_str(&a),
                | Err(e) => {
                    err_output = Some(format!("Error parsing file: {}", e));
                    break;
                }
            }
        }

        match render_line(&current_line, indent, ascii_only, size) {
            | Ok(a) => {
                indent = a.0;

                if a.1.trim_end_matches("\n").contains("\n") || &a.1 == "\n" {
                    output.push_str(&a.1);
                } else {
                    // Line wrapping
                    let mut words: Vec<&str> = a.1.split_whitespace().collect();
                    let mut output_line = "\t".repeat(indent as usize);
                    while !words.is_empty() {
                        if !words.is_empty() {
                            if get_len(&words[0]) >= size {
                                output.push_str(&words[0]);
                                output.push('\n');
                                words.remove(0);
                                break;
                            }
                            while !words.is_empty() && get_len(&output_line) + get_len(&words[0]) <= size {
                                output_line.push_str(&words[0]);
                                output_line.push(' ');
                                words.remove(0);
                            }

                            // get rid of the trailing space
                            output.push_str(&output_line[0 .. output_line.len() - 1]);
                            output.push('\n');

                            output_line = "\t".repeat(indent as usize);
                        }
                    }
                }
            },
            | Err(e) => {
                err_output = Some(format!("Error parsing file: {}", e));
                break;
            }
        }
        j += 1;
    }

    match err_output {
        | Some(e) => Err(e),
        | None => Ok(output)
    }
}

fn render_table(mut lines: Vec<Line>, indentation: u8, ascii_only: bool) -> Result<String, String> {
    for i in 0 .. lines.len() {
        if lines[i].l_type != "h" && lines[i].l_type != "r" {
            lines.remove(i);
        }
    }

    let indent_print = "\t".repeat(indentation as usize);

    // Format each row
    for i in lines.iter_mut() {
        let mut l_content = check_for('*', i.l_content.to_owned(), "\x1b[3m", "\x1b[23m");
        l_content = check_for('^', l_content, "\x1b[1m", "\x1b[22m");
        l_content = check_for('_', l_content, "\x1b[4m", "\x1b[24m");
        l_content = check_for('~', l_content, "\x1b[9m", "\x1b[29m");
        // This will help split rows into columns.
        let l_content = check_for('|', l_content, "<td>", "<td>").trim_end_matches("<td>").to_string();
        i.l_content = l_content;
    }

    let mut table: Vec<Vec<String>> = Vec::new();

    for row in lines {
        let row_as_vec: Vec<&str> = row.l_content.split("<td>").collect();
        let row_as_vec = vec_str_to_string(row_as_vec);
        table.push(row_as_vec);
    }

    let mut table_columns_num = 0;

    for i in &table {
        if i.len() > table_columns_num {
            table_columns_num = i.len();
        }
    }

    if table_columns_num != 0 {
        for col in 0 .. table_columns_num {
            let mut max_len = 0;
            for i in &table {
                if get_len(&i[col]) > max_len {
                    max_len = get_len(&i[col]);
                }
            }
            for i in table.iter_mut() {
                while get_len(&i[col]) < max_len {
                    i[col].push(' ');
                }
            }
        }
    }

    let mut table_top = "┌".to_owned();

    for col in 0 .. table_columns_num {
        for _ in 0 .. get_len(&table[0][col]) {
            table_top.push('─');
        }
        table_top.push('┬');
    }
    table_top.pop();
    table_top.push('┐');

    // The output for this function
    let mut output = String::new();

    output.push_str(&format!("{}{}\n", indent_print, table_top));

    let mut table_print = Vec::new();

    for i in &table {
        let row = i.join("│");
        table_print.push(format!("│{}│", row));
        table_print.push(table_top.replace("┌", "├").replace("┬", "┼").replace("┐", "┤").to_owned());
    }

    table_print.pop();

    for i in table_print {
        output.push_str(&format!("{}{}\n", indent_print, i));
    }

    output.push_str(&format!("{}{}\n", indent_print, table_top.replace("┌", "└").replace("┬", "┴").replace("┐", "┘")));

    if ascii_only {
        Ok(output.replace("┌", "+")
                 .replace("┬", "+")
                 .replace("┐", "+")
                 .replace("├", "+")
                 .replace("┼", "+")
                 .replace("┤", "+")
                 .replace("└", "+")
                 .replace("┴", "+")
                 .replace("┘", "+")
                 .replace("─", "-")
                 .replace("│", "|"))
    } else {
        Ok(output)
    }
}

fn render_line(line: &Line, mut indentation: u8, ascii_only: bool, size: usize) -> Result<(u8, String), String> {
    let indent_print = "\t".repeat(indentation as usize);
    let mut l_content = check_for('*', line.l_content.to_owned(), "\x1b[3m", "\x1b[23m");
    l_content = check_for('^', l_content, "\x1b[1m", "\x1b[22m");
    l_content = check_for('_', l_content, "\x1b[4m", "\x1b[24m");
    l_content = check_for('~', l_content, "\x1b[9m", "\x1b[29m");
    let print = match line.l_type {
        | "a" => Ok(format!("\x1b[31m{}{}\x1b[39m\n", indent_print, l_content)),
        | "b" => Ok(format!("\x1b[32m{}{}\x1b[39m\n", indent_print, l_content)),
        | "c" => Ok(format!("\x1b[33m{}{}\x1b[39m\n", indent_print, l_content)),
        | "d" => Ok(format!("\x1b[34m{}{}\x1b[39m\n", indent_print, l_content)),
        | "e" => Ok(format!("\x1b[35m{}{}\x1b[39m\n", indent_print, l_content)),
        | "f" => Ok(format!("\x1b[36m{}{}\x1b[39m\n", indent_print, l_content)),
        | "p" => Ok(format!("{}{}\n", indent_print, l_content)),
        // Read the file path, and then write the unmodified file slightly dimmed.
        | "m" => {
            let mut file_path = Path::new(&line.l_content);
            let str_path = format!("{}/{}", env::current_dir().unwrap().display(), line.l_content);
            if file_path.is_relative() {
                file_path = Path::new(&str_path);
            }
            let indent_line = format!("\n{}", indent_print);
            let file_content = match fs::read_to_string(file_path) {
                | Ok(a) => Ok(format!("\n\x1b[2m{}{}\x1b[22m\n", indent_print, a.replace("\n", &indent_line))),
                | Err(e) => Err(format!("Could not read file {}: {}", file_path.display(), e))
            };
            file_content
        },
        | "*" => {
            if ascii_only {
                Ok(format!("{}* {}\n", indent_print, l_content))
            } else {
                Ok(format!("{}• {}\n", indent_print, l_content))
            }
        },
        | "-" => {
            if ascii_only {
                Ok(format!("{}\n", "-".repeat(size)))
            } else {
                Ok(format!("{}\n", "─".repeat(size)))
            }
        },
        | "#" | "" => Ok(String::new()),
        | ">" => {
            indentation += 1;
            Ok(String::new())
        },
        | "<" => {
            if indentation != 0 {
                indentation -= 1;
            }
            Ok(String::new())
        },
        | "l" => Ok(format!("{}\x1b[4m{}\x1b[24m\n", indent_print, line.l_content)),
        // Start and quit table
        | "q" => Ok(String::new()),
        // Checked checkbox
        | "y" => {
            if ascii_only {
                Ok(format!("{}[X] {}\n", indent_print, l_content))
            } else {
                Ok(format!("{}☒ {}\n", indent_print, l_content))
            }
        },
        // Unchecked checkbox
        | "n" => {
            if ascii_only {
                Ok(format!("{}[ ] {}\n", indent_print, l_content))
            } else {
                Ok(format!("{}☐ {}\n", indent_print, l_content))
            }
        },
        | "\\" => Ok("\n".to_string()),
        | a => Err(format!("starting char not covered: {}", a))
    };
    match print {
        | Ok(a) => Ok((indentation, a)),
        | Err(e) => Err(e)
    }
}

#[derive(Clone)]
struct Line<'a> {
    l_type:    &'a str,
    l_content: String
}

fn check_for(pattern: char, mut content: String, replace_first: &str, replace_next: &str) -> String {
    if content.contains(pattern) {
        if content.contains('\\') {
            let mut j = 0;
            let mut split_by_slash: Vec<String> = vec_str_to_string(content.split("\\\\").collect());
            for i in split_by_slash.iter_mut() {
                let mut split_by_pattern: Vec<String> = vec_str_to_string(i.split(pattern).collect());
                for k in 0 .. split_by_pattern.len() - 1 {
                    // If the last character is not a \ then replace it with the replace-text,
                    // otherwise replace it with the pattern
                    if !split_by_pattern[k].ends_with('\\') {
                        split_by_pattern[k + 1] = format!("{}{}",
                                                          match j % 2 {
                                                              | 0 => replace_first,
                                                              | _ => replace_next
                                                          },
                                                          split_by_pattern[k + 1]);
                        j += 1;
                    } else {
                        split_by_pattern[k + 1] = format!("{}{}", pattern, split_by_pattern[k + 1]);
                    }
                    split_by_pattern[k] = split_by_pattern[k].trim_end_matches('\\').to_owned();
                }
                *i = split_by_pattern.join("");
            }
            format!("{}{}", split_by_slash.join("\\\\").replace("\\\\", "\\"), replace_next)
        } else {
            let mut j = 0;
            let mut split_by_pattern: Vec<String> = vec_str_to_string(content.split(pattern).collect());
            for k in 0 .. split_by_pattern.len() - 1 {
                // If the last character is not a \ then replace it with the replace-text,
                // otherwise replace it with the pattern
                if !split_by_pattern[k].ends_with('\\') {
                    split_by_pattern[k + 1] = format!("{}{}",
                                                      match j % 2 {
                                                          | 0 => replace_first,
                                                          | _ => replace_next
                                                      },
                                                      split_by_pattern[k + 1]);
                    j += 1;
                } else {
                    split_by_pattern[k + 1] = format!("{}{}", pattern, split_by_pattern[k + 1]);
                }
                split_by_pattern[k] = split_by_pattern[k].trim_end_matches('\\').to_owned();
            }
            content = split_by_pattern.join("");
            format!("{}{}", content, replace_next)
        }
    } else {
        content
    }
}

fn vec_str_to_string(vec_str: Vec<&str>) -> Vec<String> {
    let mut vec_string = Vec::new();
    for i in vec_str {
        vec_string.push(i.to_owned());
    }
    vec_string
}

fn get_len(string: &str) -> usize {
    // remove styling
    let string = string.replace("\x1b[1m", "")
                       .replace("\x1b[3m", "")
                       .replace("\x1b[4m", "")
                       .replace("\x1b[9m", "")
                       .replace("\x1b[22m", "")
                       .replace("\x1b[23m", "")
                       .replace("\x1b[24m", "")
                       .replace("\x1b[29m", "")
                       .replace("\t", "        ");
    string.len()
}

fn get_size() -> usize {
    unsafe {
        // If STDOUT is not a shell, set the length to 80
        // Otherwise try to find the terminal size
        // This way we can pipe stuff into less without having it only print the first word
        if isatty(STDOUT_FILENO) == 0 {
            80
        } else {
            // This is way simpler than I remember...
            let mut win_size = winsize { ws_row: 0, ws_col: 0, ws_xpixel: 0, ws_ypixel: 0 };
            ioctl(STDOUT_FILENO, TIOCGWINSZ, &mut win_size);
            win_size.ws_col as usize
        }
    }
}
