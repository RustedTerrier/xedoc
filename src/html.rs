// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
use std::{env, fs, path::Path};

fn main() {
    let mut args: Vec<String> = env::args().collect();
    if args.len() >= 1 {
        args.remove(0);
        let file_content = match fs::read_to_string(&args[0]) {
            | Ok(a) => a,
            | Err(e) => {
                eprintln!("{}", e);
                std::process::exit(1);
            }
        };

        let title = if args.len() >= 2 { &args[1] } else { "" };

        let favicon_path = if args.len() >= 3 { &args[2] } else { "" };

        match render(&file_content, Some(title), Some(favicon_path)) {
            | Ok(a) => println!("{}", a),
            | Err(e) => {
                eprintln!("{}", e);
                std::process::exit(1);
            }
        };
    } else {
        println!("Usage: xedoc_render path/to/file.xedoc [title] [path/to/favicon]");
        std::process::exit(1);
    }
}

pub fn render(contents: &str, title: Option<&str>, favicon_path: Option<&str>) -> Result<String, String> {
    let mut output = String::new();

    output.push_str("<!DOCTYPE html>\n<html>\n");

    match title {
        | Some("") | None => (),
        | Some(a) => output = format!("{}<title>{}</title>\n", output, a)
    }

    match favicon_path {
        | Some("") | None => (),
        | Some(a) => output = format!("{}<link rel=\"icon\" type=\"image/x-icon\" href=\"{}\">\n", output, a)
    }

    let mut err_output = None;
    let lines: Vec<&str> = contents.split('\n').collect();
    let mut line_syn = Vec::new();
    for i in lines {
        let mut line: Vec<&str> = i.split(' ').collect();
        let l_type = line[0];
        line.remove(0);
        let l_content = match line.is_empty() {
            | false => line.join(" "),
            | true => String::new()
        };
        line_syn.push(Line { l_type, l_content });
    }
    let mut indent = 0;
    for i in line_syn {
        match render_line(i, indent) {
            | Ok(a) => {
                indent = a.0;
                output.push_str(&a.1);
            },
            | Err(e) => {
                err_output = Some(format!("Error parsing file: {}", e));
                break;
            }
        }
    }
    output.push_str("</html>");
    if err_output == None {
        Ok(output)
    } else {
        match err_output {
            | Some(e) => Err(e),
            | None => Err(String::new())
        }
    }
}

fn render_line(line: Line, mut indentation: u8) -> Result<(u8, String), String> {
    let indent_print = format!(" style = \"text-indent: {}px\"", 40 * indentation);
    let mut l_content = check_for('*', line.l_content.clone(), "<i>", "</i>");
    l_content = check_for('^', l_content, "<b>", "</b>");
    l_content = check_for('_', l_content, "<u>", "</u>");
    l_content = check_for('~', l_content, "<s>", "</s>");
    let print = match line.l_type {
        | "a" => Ok(format!("<h1{}>{}</h1>\n", indent_print, l_content)),
        | "b" => Ok(format!("<h2{}>{}</h2>\n", indent_print, l_content)),
        | "c" => Ok(format!("<h3{}>{}</h3>\n", indent_print, l_content)),
        | "d" => Ok(format!("<h4{}>{}</h4>\n", indent_print, l_content)),
        | "e" => Ok(format!("<h5{}>{}</h5>\n", indent_print, l_content)),
        | "f" => Ok(format!("<h6{}>{}</h6>\n", indent_print, l_content)),
        | "p" => Ok(format!("<p{}>{}</p>\n", indent_print, l_content)),
        // Here I read the file, replace line endings with <br> so they appear in html, and make
        // the font monospace by <tt>
        | "m" => {
            let mut file_path = Path::new(&line.l_content);
            let str_path = format!("{}/{}", env::current_dir().unwrap().display(), line.l_content);
            if file_path.is_relative() {
                file_path = Path::new(&str_path);
            }
            let file_content = match fs::read_to_string(file_path) {
                | Ok(a) => Ok(format!("<p{}><tt><br>{}</tt></p>\n", indent_print, a.replace('\n', "<br>"))),
                | Err(e) => Err(format!("Could not read file {}: {}", file_path.display(), e))
            };
            file_content
        },
        | "*" => Ok(format!("<li{}>{}</li>\n", indent_print, l_content)),
        | "-" => Ok("<hr>\n".to_owned()),
        | "#" | "" => Ok(String::new()),
        | ">" => {
            indentation += 1;
            Ok(String::new())
        },
        | "<" => {
            if indentation != 0 {
                indentation -= 1;
            }
            Ok(String::new())
        },
        | "l" => Ok(format!("<p{}><a href=\"{}\">{}</a></p>\n", indent_print, line.l_content, line.l_content)),
        // Start and quit table
        | "s" => Ok(format!("<table style=\"margin-left:{}px\" border=\"1 | 0\">\n", indentation * 40)),
        | "q" => Ok("</table>\n".to_string()),
        // Table row
        | "r" => Ok(format!("<tr>\n<td>{}</tr>\n", check_for('|', l_content, "</td>\n<td>", "</td>\n<td>").trim_end_matches("\n<td>"))),
        // Table header
        | "h" => Ok(format!("<tr>\n<th>{}</tr>\n", check_for('|', l_content, "</th>\n<th>", "</th>\n<th>").trim_end_matches("\n<th>"))),
        // Checked checkbox
        | "y" => Ok(format!("<p{}><input type=\"checkbox\" disabled=\"disabled\" checked=\"true\"> {}<br></p>", indent_print, l_content)),
        // Unchecked checkbox
        | "n" => Ok(format!("<p{}><input type=\"checkbox\" disabled=\"disabled\"> {}<br></p>", indent_print, l_content)),
        // New line
        | "\\" => Ok("<p></p>\n".to_string()),
        | a => Err(format!("starting char not covered: {}", a))
    };
    match print {
        | Ok(a) => Ok((indentation, a)),
        | Err(e) => Err(e)
    }
}

struct Line<'a> {
    l_type:    &'a str,
    l_content: String
}

fn check_for(pattern: char, mut content: String, replace_first: &str, replace_next: &str) -> String {
    if content.contains(pattern) {
        if content.contains('\\') {
            let mut j = 0;
            let mut split_by_slash: Vec<String> = vec_str_to_string(content.split("\\\\").collect());
            for i in split_by_slash.iter_mut() {
                let mut split_by_pattern: Vec<String> = vec_str_to_string(i.split(pattern).collect());
                for k in 0 .. split_by_pattern.len() - 1 {
                    // If the last character is not a \ then replace it with the replace-text,
                    // otherwise replace it with the pattern
                    if !split_by_pattern[k].ends_with('\\') {
                        split_by_pattern[k + 1] = format!("{}{}",
                                                          match j % 2 {
                                                              | 0 => replace_first,
                                                              | _ => replace_next
                                                          },
                                                          split_by_pattern[k + 1]);
                        j += 1;
                    } else {
                        split_by_pattern[k + 1] = format!("{}{}", pattern, split_by_pattern[k + 1]);
                    }
                    split_by_pattern[k] = split_by_pattern[k].trim_end_matches('\\').to_owned();
                }
                *i = split_by_pattern.join("");
            }
            format!("{}{}", split_by_slash.join("\\\\").replace("\\\\", "\\"), replace_next)
        } else {
            let mut j = 0;
            let mut split_by_pattern: Vec<String> = vec_str_to_string(content.split(pattern).collect());
            for k in 0 .. split_by_pattern.len() - 1 {
                // If the last character is not a \ then replace it with the replace-text,
                // otherwise replace it with the pattern
                if !split_by_pattern[k].ends_with('\\') {
                    split_by_pattern[k + 1] = format!("{}{}",
                                                      match j % 2 {
                                                          | 0 => replace_first,
                                                          | _ => replace_next
                                                      },
                                                      split_by_pattern[k + 1]);
                    j += 1;
                } else {
                    split_by_pattern[k + 1] = format!("{}{}", pattern, split_by_pattern[k + 1]);
                }
                split_by_pattern[k] = split_by_pattern[k].trim_end_matches('\\').to_owned();
            }
            content = split_by_pattern.join("");
            format!("{}{}", content, replace_next)
        }
    } else {
        content
    }
}

fn vec_str_to_string(vec_str: Vec<&str>) -> Vec<String> {
    let mut vec_string = Vec::new();
    for i in vec_str {
        vec_string.push(i.to_owned());
    }
    vec_string
}
