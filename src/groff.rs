// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
use std::{env, fs, path::Path};

fn main() {
    let mut args: Vec<String> = env::args().collect();
    args.remove(0);
    match render(fs::read_to_string(&args[0]).unwrap()) {
        | Err(e) => {
            eprintln!("{}", e);
            std::process::exit(1);
        },
        | Ok(a) => println!("{}", a)
    };
}

pub fn render(contents: String) -> Result<String, String> {
    let mut output = String::new();
    let mut err_output = None;
    let lines: Vec<&str> = contents.split('\n').collect();
    let mut line_syn = Vec::new();
    for i in lines {
        let mut line: Vec<&str> = i.split(' ').collect();
        let l_type = line[0];
        line.remove(0);
        let l_content = match line.is_empty() {
            | false => line.join(" "),
            | true => String::new()
        };
        line_syn.push(Line { l_type, l_content });
    }
    let mut indentation = 0;
    let mut j = 0;
    while j < line_syn.len() {
        let mut i = line_syn[j].clone();
        if i.l_type == "s" {
            let mut table_syn = Vec::new();
            while i.l_type != "q" {
                j += 1;
                i = line_syn[j].clone();
                table_syn.push(i.clone());
            }
            table_syn.pop();
            match render_table(table_syn) {
                | Ok(a) => output.push_str(&a),
                | Err(e) => {
                    err_output = Some(format!("Error parsing file: {}", e));
                    break;
                }
            }
        }
        match render_line(i, indentation) {
            | Ok(a) => {
                output.push_str(&a.0);
                indentation = a.1;
            },
            | Err(e) => {
                err_output = Some(format!("Error parsing file: {}", e));
                break;
            }
        }
        j += 1;
    }
    if err_output == None {
        Ok(output)
    } else {
        match err_output {
            | Some(e) => Err(e),
            | None => Err(String::new())
        }
    }
}

fn render_table(mut lines: Vec<Line>) -> Result<String, String> {
    for i in 0 .. lines.len() {
        if lines[i].l_type != "h" && lines[i].l_type != "r" {
            lines.remove(i);
        }
    }

    // Format each row
    for i in lines.iter_mut() {
        let mut l_content = i.l_content.clone();
        l_content = l_content.replace("\\\\", "\\e");
        l_content = check_for('*', l_content, "\\fI", "\\fR");
        l_content = check_for('^', l_content, "\\fB", "\\fR");
        // It also underlines in my setup and I don't know how to only just underline.
        l_content = check_for('_', l_content, "\\fI", "\\fR");
        // Unsupported
        l_content = check_for('~', l_content, "", "");
        // This will help split rows into columns.
        let l_content = check_for('|', l_content, "<td>", "<td>").trim_end_matches("<td>").to_string();
        i.l_content = l_content;
    }

    let mut table: Vec<Vec<String>> = Vec::new();

    for row in lines {
        let row_as_vec: Vec<&str> = row.l_content.split("<td>").collect();
        let row_as_vec = vec_str_to_string(row_as_vec);
        table.push(row_as_vec);
    }

    let mut table_columns_num = 0;

    for i in &table {
        if i.len() > table_columns_num {
            table_columns_num = i.len();
        }
    }

    // The output for this function
    let mut output = ".TS\nallbox tab(:);\n".to_string();
    output.push_str(&"l ".repeat(table_columns_num));
    output.push_str(".\n");

    for i in &table {
        for j in i {
            output.push_str(j);
            output.push(':');
        }
        output.push('\n');
    }
    output.push_str(".TE\n");

    Ok(output)
}

fn render_line(line: Line, mut indentation: u8) -> Result<(String, u8), String> {
    let mut l_content = line.l_content.clone();
    l_content = l_content.replace("\\\\", "\\e");
    l_content = check_for('*', l_content, "\\fI", "\\fR");
    l_content = check_for('^', l_content, "\\fB", "\\fR");
    // It also underlines in my setup and I don't know how to only just underline.
    l_content = check_for('_', l_content, "\\fI", "\\fR");
    // Unsupported
    l_content = check_for('~', l_content, "", "");
    let print = match line.l_type {
        | "a" => Ok(format!(".SH {}\n", l_content)),
        | "b" => Ok(format!(".SS {}\n", l_content)),
        | "c" => Ok(format!(".SS {}\n", l_content)),
        | "d" => Ok(format!(".SS {}\n", l_content)),
        | "e" => Ok(format!(".SS {}\n", l_content)),
        | "f" => Ok(format!(".SS {}\n", l_content)),
        | "p" => Ok(format!(".P\n.in +{}\n{}\n", indentation * 5, l_content)),
        | "m" => {
            let mut file_path = Path::new(&line.l_content);
            let str_path = format!("{}/{}", env::current_dir().unwrap().display(), line.l_content);
            if file_path.is_relative() {
                file_path = Path::new(&str_path);
            }
            let file_content = match fs::read_to_string(file_path) {
                | Ok(a) => Ok(format!(".nf\n{}.ns\n", a)),
                | Err(e) => Err(format!("Could not read file {}: {}", file_path.display(), e))
            };
            file_content
        },
        | "*" => Ok(format!(".IP \\[bu]\n{}\n", l_content)),
        | "-" => Ok(".HR\n".to_owned()),
        | "#" | "" => Ok(String::new()),
        // Indent and unindent
        | ">" => {
            indentation += 1;
            Ok(String::new())
        },
        | "<" => {
            if indentation != 0 {
                indentation -= 1;
            }
            Ok(String::new())
        },
        | "l" => Ok(format!(".UR {}\n", l_content.replace(":", "\\:"))),
        // Start and quit table
        | "s" => Ok(".TS\nallbox tab(:);\n.\n".to_string()),
        | "q" => Ok(".TE\n".to_string()),
        // Table row
        | "r" => Ok(format!("{}\n_\n", check_for('|', l_content.replace(":", "\\:"), ":", ":").trim_end_matches(":"))),
        // Table header
        | "h" => Ok(format!("{}\n_\n", check_for('|', l_content.replace(":", "\\:"), ":", ":").trim_end_matches(":"))),
        // Checked checkbox
        | "y" => Ok(format!("[X]\t{}\n.br\n", l_content)),
        // Unchecked checkbox
        | "n" => Ok(format!("[ ]\t{}\n.br\n", l_content)),
        // New line
        | "\\" => Ok(".P\n\n".to_string()),
        | a => Err(format!("starting char not covered: {}", a))
    };
    match print {
        | Ok(a) => Ok((a, indentation)),
        | Err(e) => Err(e)
    }
}

#[derive(Clone)]
struct Line<'a> {
    l_type:    &'a str,
    l_content: String
}

fn check_for(pattern: char, mut content: String, replace_first: &str, replace_next: &str) -> String {
    if content.contains(pattern) {
        if content.contains('\\') {
            let mut j = 0;
            let mut split_by_slash: Vec<String> = vec_str_to_string(content.split("\\\\").collect());
            for i in split_by_slash.iter_mut() {
                let mut split_by_pattern: Vec<String> = vec_str_to_string(i.split(pattern).collect());
                for k in 0 .. split_by_pattern.len() - 1 {
                    // If the last character is not a \ then replace it with the replace-text,
                    // otherwise replace it with the pattern
                    if !split_by_pattern[k].ends_with('\\') {
                        split_by_pattern[k + 1] = format!("{}{}",
                                                          match j % 2 {
                                                              | 0 => replace_first,
                                                              | _ => replace_next
                                                          },
                                                          split_by_pattern[k + 1]);
                        j += 1;
                    } else {
                        split_by_pattern[k + 1] = format!("{}{}", pattern, split_by_pattern[k + 1]);
                    }
                    split_by_pattern[k] = split_by_pattern[k].trim_end_matches('\\').to_owned();
                }
                *i = split_by_pattern.join("");
            }
            format!("{}{}", split_by_slash.join("\\\\").replace("\\\\", "\\"), replace_next)
        } else {
            let mut j = 0;
            let mut split_by_pattern: Vec<String> = vec_str_to_string(content.split(pattern).collect());
            for k in 0 .. split_by_pattern.len() - 1 {
                // If the last character is not a \ then replace it with the replace-text,
                // otherwise replace it with the pattern
                if !split_by_pattern[k].ends_with('\\') {
                    split_by_pattern[k + 1] = format!("{}{}",
                                                      match j % 2 {
                                                          | 0 => replace_first,
                                                          | _ => replace_next
                                                      },
                                                      split_by_pattern[k + 1]);
                    j += 1;
                } else {
                    split_by_pattern[k + 1] = format!("{}{}", pattern, split_by_pattern[k + 1]);
                }
                split_by_pattern[k] = split_by_pattern[k].trim_end_matches('\\').to_owned();
            }
            content = split_by_pattern.join("");
            format!("{}{}", content, replace_next)
        }
    } else {
        content
    }
}

fn vec_str_to_string(vec_str: Vec<&str>) -> Vec<String> {
    let mut vec_string = Vec::new();
    for i in vec_str {
        vec_string.push(i.to_owned());
    }
    vec_string
}
