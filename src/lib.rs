// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
mod html;
mod render;
// Maybe it should be called roff...
mod groff;

pub fn html(contents: &str, title: Option<&str>, favicon_path: Option<&str>) -> Result<String, String> {
    html::render(contents, title, favicon_path)
}

pub fn render(contents: String, ascii_only: bool, size: Option<usize>) -> Result<String, String> {
    render::render(contents, ascii_only, size)
}

pub fn groff(contents: String) -> Result<String, String> {
    groff::render(contents)
}
